import React from 'react';
import './App.css';
import update_person from './store/actions/personActions'
import update_game from './store/actions/gameActions'
import {connect} from 'react-redux'
import fetch_user from './store/actions/userActions'

function App(props) {
  return (
    <div className="App">
      <h1>Redux Tutorial</h1>
      person name : {props.person}
      <button onClick={props.updatePerson}>update_person</button>
      <br/>
      game name : {props.game}
      <button onClick={props.updateGame}>update_person</button>
      <br/>
      users:  <button onClick={props.fetchUser}>Get user list</button>
      {
        props.users.length ===0?<p>No users found</p>:
      props.users.map(user => <p key={user.id}>{user.id}--{user.first_name}</p>)
      }
    </div>
  );
}
const mapStateToProps = state =>{
  return {
    person:state.person.name,
    game:state.game.name,
    users:state.users
  }
}
const mapDispatchToProps = dispatch=>{
  return{
    updatePerson : ()=> {dispatch(update_person)},
    updateGame:()=>{dispatch(update_game)},
    fetchUser:()=>{dispatch(fetch_user)}
  }
}
export default connect(mapStateToProps,mapDispatchToProps) (App);
