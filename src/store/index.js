import {createStore, combineReducers, applyMiddleware, compose} from 'redux'

import gameReducer from './reducers/gameReducer'
import personReducer from './reducers/personReducer'
import userReducer from './reducers/userReducer'
import thunk from 'redux-thunk'


const AllReducer = combineReducers({person:personReducer ,game:gameReducer,users:userReducer})
const IntialStates ={
  game:{name:'cricket'},
  person:{name:'arya' , email:'arya@gmail.com'},
  users:[]
 
}
const middleware =[thunk]
const store = createStore(AllReducer,IntialStates,compose(applyMiddleware(...middleware),
window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
  )


export default store;
